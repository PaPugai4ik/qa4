package org.itstep.qa.test;

import org.itstep.qa.other.DataProviderClass;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Rambler {
    private WebDriver webDriver;
    private WebElement element;
    @BeforeClass
    public void createWebDriver(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        webDriver =new ChromeDriver();
        webDriver.manage().window().maximize();
    }
    @BeforeMethod
    public void openStart(){
        webDriver.get("https://id.rambler.ru/login-20/mail-registration?back=https%3A%2F%2Fwww.rambler.ru%2F&rname=head&param=iframe&iframeOrigin=https%3A%2F%2Fwww.rambler.ru");
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "validationPassword")
    public void validationPassword(String value,String v){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element = webDriver.findElement(By.xpath("//*[@id=\"newPassword\"]"));
        element.sendKeys(value);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div"));
        element.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String text = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div")).getText();
        Assert.assertEquals(text,v);
    }
    @Test
    public void validationPasswordLatin(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element = webDriver.findElement(By.xpath("//*[@id=\"newPassword\"]"));
        element.sendKeys("Kill@#$123");
        element = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div"));
        element.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String text = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div")).getText();
        Assert.assertEquals(text,"Символ \"#\" не поддерживается. Можно использовать символы ! @ $ % ^ & * ( ) _ - +");
    }
    @Test
    public void validationPasswordRussianLetters(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element = webDriver.findElement(By.xpath("//*[@id=\"newPassword\"]"));
        element.sendKeys("йцуйцуйцу");
        element = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div"));
        element.click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String text = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div")).getText();
        Assert.assertEquals(text,"Вы вводите русские буквы");
    }
    //Проверка поролей
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "password")
    public void testMatchesPassword(String value){
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element = webDriver.findElement(By.xpath("//*[@id=\"newPassword\"]"));
        element.sendKeys(value);
        String newPassword =webDriver.findElement(By.xpath("//*[@id=\"newPassword\"]")).getAttribute("value");
        element = webDriver.findElement(By.xpath("//*[@id=\"confirmPassword\"]"));
        element.sendKeys(value);
        String confirmPassword =webDriver.findElement(By.xpath("//*[@id=\"confirmPassword\"]")).getAttribute("value");
        Assert.assertEquals(newPassword,confirmPassword);
    }
    //Валидация поле Email
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "email")
    public void testValidationEmail(String value){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        element =webDriver.findElement(By.xpath("//*[@id=\"login\"]"));
        element.sendKeys(value);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    @AfterClass
    public void shutdown(){
        webDriver.manage().deleteAllCookies();
        webDriver.close();
    }
}
