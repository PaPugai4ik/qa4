package org.itstep.qa.other;

import org.testng.annotations.DataProvider;

    public class DataProviderClass {
        @DataProvider(name = "phone")
        public Object[][]dataProviderPhone(){
            return new Object[][]{{"aasdd"},{"!@#%^"},{"213123"}};
        }
        @DataProvider(name = "email")
        public Object[][]dataProviderAdress(){
            return new Object[][]{
                    {"BALYSH"},
                    {"fdasdasda"},
                    {"fdfdfd2123"},
                    {"balysh"},
                    {"balysh-1993"},
                    {"balysh1993"}};
        }
        @DataProvider(name = "validationPassword")
        public Object[][] dataProviderValidationPassword(){
            return new Object[][]{
                    {"dsdfgjjojf2123453ufbdsdfgjjojf2123453ufb","Пароль должен содержать от 8 до 32 символов, включать хотя бы одну заглавную латинскую букву, одну строчную и одну цифру"},{"ddhhe21","Пароль должен содержать от 8 до 32 символов, включать хотя бы одну заглавную латинскую букву, одну строчную и одну цифру"},
                    {"123456789","Пароль должен содержать от 8 до 32 символов, включать хотя бы одну заглавную латинскую букву, одну строчную и одну цифру"}};
        }
        @DataProvider(name = "password")
        public Object[][] dataProviderPassword(){
            return new Object[][]{
                    {"Qwer1234"},{"Qwer1234qwe"}};
        }

    }

