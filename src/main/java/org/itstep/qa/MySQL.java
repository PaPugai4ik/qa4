package org.itstep.qa;

import java.sql.*;
import java.util.Scanner;

public class MySQL {
    public static void main(String[] args) {
        Connection connection=null;
        Statement stat = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/world?serverTimezone=UTC", "root", "root");
            String sql = "SELECT * FROM city Where CountryCode = ? ";

            preparedStatement = connection.prepareStatement(sql);

            Scanner scanner = new Scanner(System.in);

            String line = scanner.nextLine();

            preparedStatement.setString(1,line);

            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                System.out.println(
                        resultSet.getString("Name")+" "
                        +resultSet.getString("CountryCode")+" "
                        +resultSet.getString("District")+" "
                        +resultSet.getString("Population"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                resultSet.close();
                preparedStatement.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }
}
