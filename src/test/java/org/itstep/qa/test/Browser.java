package org.itstep.qa.test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class Browser {
    @Test
    public void Chrome(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriver webDriver =new ChromeDriver();
        webDriver.get("https://www.onliner.by/");
        webDriver.manage().window().maximize();
    }
    @Test
    public void Firefox(){
        System.setProperty("webdriver.gecko.driver","src/main/resources/geckodriver.exe");
        WebDriver webDriver = new FirefoxDriver();
        webDriver.get("https://www.onliner.by/");
        webDriver.manage().window().maximize();
    }
}
