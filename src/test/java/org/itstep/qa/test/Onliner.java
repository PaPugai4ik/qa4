package org.itstep.qa.test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;


import java.util.concurrent.TimeUnit;

public class Onliner {
    @Test
    public void InputOnlinerRegestEmail(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriver webDriver =new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("https://www.onliner.by");
        String text = webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/div/div[1]/div/div[1]/ul/li[3]/a/span/span")).getText();
        Assert.assertEquals(text,"Автомобильные шины");
        webDriver.findElement(By.xpath("//*[@id=\"userbar\"]/div/div/div/div[1]")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/div[1]")).getText();
        Assert.assertEquals(text,"Вход");
        text=webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[4]/div[1]/a")).getText();
        Assert.assertEquals(text,"Зарегистрироваться на Onliner");
        webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[4]/div[1]/a")).click();
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input")).sendKeys("bbbb.ru");
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div/div/input")).sendKeys("killerq123456");
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input")).sendKeys("killerq123456");
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input")).sendKeys(Keys.ENTER);
        text=webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div")).getText();
        Assert.assertEquals(text,"Некорректный e-mail");
        webDriver.close();
    }
    @Test
    public void InputOnlinerRegestNotPassword(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriver webDriver =new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("https://www.onliner.by");
        String text = webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/div/div[1]/div/div[1]/ul/li[3]/a/span/span")).getText();
        Assert.assertEquals(text,"Автомобильные шины");
        webDriver.findElement(By.xpath("//*[@id=\"userbar\"]/div/div/div/div[1]")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/div[1]")).getText();
        Assert.assertEquals(text,"Вход");
        text=webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[4]/div[1]/a")).getText();
        Assert.assertEquals(text,"Зарегистрироваться на Onliner");
        webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[4]/div[1]/a")).click();
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div/div/input")).sendKeys("bbbb@mail.ru");
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div/div/input")).sendKeys("killerq12346");
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input")).sendKeys("killerq123456");
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input")).sendKeys(Keys.ENTER);
        text=webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div")).getText();
        Assert.assertEquals(text,"Пароли не совпадают");
        webDriver.close();
    }
    @Test
    public void InputOnlinerRegestrNull(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriver webDriver =new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("https://www.onliner.by");
        String text = webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/div/div[1]/div/div[1]/ul/li[3]/a/span/span")).getText();
        Assert.assertEquals(text,"Автомобильные шины");
        webDriver.findElement(By.xpath("//*[@id=\"userbar\"]/div/div/div/div[1]")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/div[1]")).getText();
        Assert.assertEquals(text,"Вход");
        text=webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[4]/div[1]/a")).getText();
        Assert.assertEquals(text,"Зарегистрироваться на Onliner");
        webDriver.findElement(By.xpath("//*[@id=\"auth-container\"]/div/div[2]/div/form/div[4]/div[1]/a")).click();
        webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input")).sendKeys(Keys.ENTER);
        text=webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div")).getText();
        Assert.assertEquals(text,"Укажите e-mail");
        text=webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div[2]/div")).getText();
        Assert.assertEquals(text,"Укажите пароль");
        text=webDriver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div")).getText();
        Assert.assertEquals(text,"Укажите пароль");
        webDriver.close();
    }
}
