package org.itstep.qa.test;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class Dev {
    @Test
    public void InputDevRegestrEmail(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriver webDriver =new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("https://dev.by/");
        String text = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/header/div/div[1]/div/nav/div/div[2]/a")).getText();
        Assert.assertEquals(text,"Вакансии");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/header/div/div[2]/div[2]/div/a")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[1]")).getText();
        Assert.assertEquals(text,"Вход");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]")).getText();
        Assert.assertEquals(text,"Регистрация");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input")).sendKeys("bbb.ru");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input")).sendKeys("killqer1234");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input")).sendKeys("Papugai4ik");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input")).sendKeys("+375444772053");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input")).sendKeys(Keys.ENTER);
        text=webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span")).getText();
        Assert.assertEquals(text,"Введите корректный адрес электронной почты.");
        webDriver.close();
    }
    @Test
    public void InputDevNull(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriver webDriver =new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("https://dev.by/");
        String text = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/header/div/div[1]/div/nav/div/div[2]/a")).getText();
        Assert.assertEquals(text,"Вакансии");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/header/div/div[2]/div[2]/div/a")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[1]")).getText();
        Assert.assertEquals(text,"Вход");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]")).getText();
        Assert.assertEquals(text,"Регистрация");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span")).getText();
        Assert.assertEquals(text,"Что-то пошло не так. Попробуйте ещё раз.");
        webDriver.close();
    }
    @Test
    public void CheckDevFields(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        WebDriver webDriver =new ChromeDriver();
        webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();
        webDriver.get("https://dev.by/");
        String text = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/header/div/div[1]/div/nav/div/div[2]/a")).getText();
        Assert.assertEquals(text,"Вакансии");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div[2]/header/div/div[2]/div[2]/div/a")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[1]")).getText();
        Assert.assertEquals(text,"Вход");
        webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]")).click();
        text=webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]")).getText();
        Assert.assertEquals(text,"Регистрация");
        text =webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input")).getAttribute("placeholder");
        Assert.assertEquals(text,"Электронная почта");
        text =webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input")).getAttribute("placeholder");
        Assert.assertEquals(text,"Пароль");
        text =webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input")).getAttribute("placeholder");
        Assert.assertEquals(text,"Юзернейм");
        text =webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input")).getAttribute("placeholder");
        Assert.assertEquals(text,"Номер телефона");
        text =webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span/span")).getText();
        Assert.assertEquals(text,"Зарегистрироваться");
        text = webDriver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button")).getTagName();
        Assert.assertEquals(text,"button");
        webDriver.close();
    }
}
