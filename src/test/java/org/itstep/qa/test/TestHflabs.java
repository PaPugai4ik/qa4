package org.itstep.qa.test;

import org.itstep.qa.other.DataProviderClass;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;


public class TestHflabs {
    private WebDriver webDriver;
    @BeforeClass
    public void createWebDriver(){
        System.setProperty("webdriver.chrome.driver","src/main/resources/chromedriver.exe");
        webDriver =new ChromeDriver();
        webDriver.manage().window().maximize();
    }
    @BeforeMethod
    public void openStart(){
        webDriver.get("http://hflabs.github.io/suggestions-demo/");
        //
    }
    @Test
    public void fieldFio(){
        WebElement element = webDriver.findElement(By.xpath("//*[@id=\"fullname\"]"));
        String text = element.getAttribute("placeholder");
        Assert.assertEquals(text,"Введите ФИО в свободной форме");
        element=webDriver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/label"));
        text=element.getText();
        Assert.assertEquals(text,"ФИО");
        element=webDriver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/span"));
        text=element.getText();
        Assert.assertEquals(text," ∗");

    }
    @Test
    public void fieldEmail(){
        WebElement element = webDriver.findElement(By.xpath("//*[@id=\"email\"]"));
        String text = element.getAttribute("placeholder");
        Assert.assertEquals(text,"me@example.com");
        element=webDriver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[4]/label"));
        text=element.getText();
        Assert.assertEquals(text,"E-mail");
        element=webDriver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[4]/span"));
        text=element.getText();
        Assert.assertEquals(text," ∗");
    }
    @Test
    public void fieldTextArea(){
        String text;
        WebElement element=webDriver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[2]/label"));
        text=element.getText();
        Assert.assertEquals(text,"Содержание обращения");
        element=webDriver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[2]/span"));
        text=element.getText();
        Assert.assertEquals(text," ∗");
    }
    //Отправка сообщений
    @Test
    public void testShipping(){
        WebElement element = webDriver.findElement(By.xpath("//*[@id=\"fullname\"]"));
        element.sendKeys("Введенская");
        element.sendKeys(Keys.SPACE);
        element.sendKeys("Валентина");
        element.sendKeys(Keys.SPACE);
        element.sendKeys("Ырысбековна");
        element.sendKeys(Keys.SPACE);
        element.click();
        element = webDriver.findElement(By.xpath("//*[@id=\"email\"]"));
        element.sendKeys("balysh@mail.ru");
        element = webDriver.findElement(By.xpath("//*[@id=\"message\"]"));
        element.sendKeys("dasasdasd");
        element.sendKeys(Keys.ENTER);
        element = webDriver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button"));
        element.click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        boolean isVisible = webDriver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
        Assert.assertTrue(isVisible);
        isVisible = webDriver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isEnabled();
        Assert.assertTrue(isVisible);


    }
    //Валидация поле Email
    @Test
    public void emptyEmailField(){
        WebElement element = webDriver.findElement(By.xpath("//*[@id=\"fullname\"]"));
        element.sendKeys("Введенская");
        element.sendKeys(Keys.SPACE);
        element.sendKeys("Валентина");
        element.sendKeys(Keys.SPACE);
        element.sendKeys("Ырысбековна");
        element.sendKeys(Keys.SPACE);
        element.click();
        element = webDriver.findElement(By.xpath("//*[@id=\"email\"]"));
        element.sendKeys(Keys.ENTER);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String text = webDriver.findElement(By.xpath("//*[@id=\"email\"]")).getAttribute("required");
        Assert.assertTrue(true,text);
    }
    @Test(dataProviderClass = DataProviderClass.class,dataProvider = "email")
    public void testEmail(String value){
        WebElement element = webDriver.findElement(By.xpath("//*[@id=\"fullname\"]"));
        element.sendKeys("Введенская");
        element.sendKeys(Keys.SPACE);
        element.sendKeys("Валентина");
        element.sendKeys(Keys.SPACE);
        element.sendKeys("Ырысбековна");
        element.sendKeys(Keys.SPACE);
        element.click();
        element = webDriver.findElement(By.xpath("//*[@id=\"email\"]"));
        element.sendKeys(value);
    }
    @Test
    public void testAdress(){
        WebElement element = webDriver.findElement(By.xpath("//*[@id=\"address\"]"));
        element.sendKeys("127");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement[]elements=webDriver.findElements(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[2]/div[1]/div/div")).toArray(new WebElement[0]);
        elements[0].click();

        String text = webDriver.findElement(By.xpath("//*[@id=\"address-postal_code\"]")).getAttribute("value");
        Assert.assertEquals(text,"109443");

        text = webDriver.findElement(By.xpath("//*[@id=\"address-region\"]")).getAttribute("value");
        Assert.assertEquals(text,"г Москва");

        text = webDriver.findElement(By.xpath("//*[@id=\"address-city\"]")).getAttribute("value");
        Assert.assertEquals(text,"г Москва");

        text = webDriver.findElement(By.xpath("//*[@id=\"address-street\"]")).getAttribute("value");
        Assert.assertEquals(text,"пр-кт Волгоградский");

        text = webDriver.findElement(By.xpath("//*[@id=\"address-house\"]")).getAttribute("value");
        Assert.assertEquals(text,"д 127, к 1");

    }
    @AfterClass
    public void shutdown(){
        webDriver.manage().deleteAllCookies();
        webDriver.close();
    }
}
